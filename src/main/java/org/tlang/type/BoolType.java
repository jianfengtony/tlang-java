package org.tlang.type;

import org.tlang.keywords.KeyWords;

public class BoolType extends Type {

    public static final BoolType BOOL_TYPE = new BoolType();

    private BoolType() {
    }

    @Override
    public boolean isTypeOf(Type type) {
        return type instanceof BoolType;
    }

    @Override
    public boolean isPrimitiveType() {
        return true;
    }

    @Override
    public String toString() {
        return KeyWords.TYPE_BOOL;
    }
}
