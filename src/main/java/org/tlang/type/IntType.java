package org.tlang.type;

import org.tlang.keywords.KeyWords;

public class IntType extends Type {

    public static final IntType INT_TYPE = new IntType();

    private IntType() {
    }

    @Override
    public boolean isTypeOf(Type type) {
        return type instanceof IntType;
    }

    @Override
    public boolean isPrimitiveType() {
        return true;
    }

    @Override
    public String toString() {
        return KeyWords.TYPE_INT;
    }
}
