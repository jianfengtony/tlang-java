package org.tlang.type;

public class FloatType extends Type {

    public static final FloatType FLOAT_TYPE = new FloatType();

    private FloatType() {
    }

    @Override
    public boolean isTypeOf(Type type) {
        return type instanceof FloatType;
    }

    @Override
    public boolean isPrimitiveType() {
        return true;
    }
}
