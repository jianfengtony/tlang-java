package org.tlang.type;

public class ReturnType extends Type {
    private final Type type;

    public ReturnType(Type type) {
        this.type = type;
    }

    public Type type() {
        return type;
    }

    @Override
    public boolean isTypeOf(Type type) {
        if (!(type instanceof ReturnType)) {
            return false;
        }
        return this.type.isTypeOf(((ReturnType) type).type);
    }

    @Override
    public String toString() {
        return "return<" + type.toString() + ">";
    }
}
