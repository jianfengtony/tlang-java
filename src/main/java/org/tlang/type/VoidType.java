package org.tlang.type;

import org.tlang.keywords.KeyWords;

public class VoidType extends Type {

    public static final VoidType VOID_TYPE = new VoidType();

    private VoidType() {
    }

    @Override
    public boolean isTypeOf(Type type) {
        return type instanceof VoidType;
    }

    @Override
    public boolean isPrimitiveType() {
        return true;
    }

    @Override
    public String toString() {
        return KeyWords.TYPE_VOID;
    }
}
