package org.tlang.type;

import org.tlang.keywords.KeyWords;

public class StrType extends Type {

    public static final StrType STR_TYPE = new StrType();

    private StrType() {
    }

    @Override
    public boolean isTypeOf(Type type) {
        return type instanceof StrType;
    }

    @Override
    public boolean isPrimitiveType() {
        return true;
    }

    @Override
    public String toString() {
        return KeyWords.TYPE_STR;
    }
}
