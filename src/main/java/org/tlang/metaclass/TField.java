package org.tlang.metaclass;

import org.tlang.context.ValueTable;

public abstract class TField implements TMember {
    private ValueTable valueTable;
    private final int nest;
    private final int index;

    public TField(int nest, int index) {
        this.nest = nest;
        this.index = index;
    }

    @Override
    public void bindValueTable(ValueTable valueTable) {
        this.valueTable = valueTable;
    }

    protected TObject valueTable() {
        return (TObject) valueTable;
    }

    protected int nest() {
        return nest;
    }

    protected int index() {
        return index;
    }
}
