package org.tlang.metaclass;

public interface TGetter {
    Object get();
}
