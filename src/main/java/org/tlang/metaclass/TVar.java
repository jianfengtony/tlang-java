package org.tlang.metaclass;

import org.tlang.context.ValueTable;

public class TVar implements TGetter, TSetter {
    private Object value;

    public TVar(Object value) {
        this.value = value;
    }

    @Override
    public Object get() {
        return value;
    }

    @Override
    public void set(ValueTable valueTable, Object value) {
        this.value = value;
    }

    public TVar copy() {
        return new TVar(value);
    }
}
