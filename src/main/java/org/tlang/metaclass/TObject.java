package org.tlang.metaclass;

import org.tlang.ast.list.clazz.ClassDefine;
import org.tlang.context.ValueTable;

public class TObject extends ValueTable {
    private final ClassDefine classDefine;

    public TObject(ClassDefine classDefine) {
        super(classDefine.classType().fieldSymbolTable().size(), classDefine.methodValuetable(),
                "object<" + classDefine.name() + ">, field");
        this.classDefine = classDefine;
        this.update(0, ClassDefine.INDEX_THIS_IN_FIELD_SYMBOLS, this);
        classDefine.initTObject(this);
    }

    @Override
    public Object get(int nest, int index) {
        Object result = super.get(nest, index);
        if (result instanceof TMethod) {
            ((TMethod) result).bindValueTable(this);
        }
        return result;
    }

    @Override
    public String toString() {
        return "<TObject: " + classDefine.name() + ">";
    }
}
