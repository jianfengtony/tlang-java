package org.tlang.metaclass;

import org.tlang.context.ValueTable;

/**
 * 可被赋值的
 */
public interface TSetter {
    void set(ValueTable valueTable, Object value);
}
