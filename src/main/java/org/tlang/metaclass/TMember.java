package org.tlang.metaclass;

import org.tlang.context.ValueTable;

public interface TMember {
    void bindValueTable(ValueTable valueTable);
}
