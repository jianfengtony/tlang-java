package org.tlang.metaclass;

import org.tlang.ast.list.func.ArgumentList;
import org.tlang.context.ValueTable;

public interface TFunction {

    Object execute(ValueTable callerValueTable, ArgumentList arguments);
}
