package org.tlang.metaclass;

public class TReturn {
    private final Object value;
    public static final Object VOID = null;

    public TReturn(Object value) {
        this.value = value;
    }

    public Object value() {
        return value;
    }
}
