package org.tlang.exception;

import org.tlang.ast.AST;

public class EvalException extends RuntimeException {

    public EvalException(String msg) {
        super(msg);
    }

    public EvalException(String msg, AST ast) {
        super(msg + " " + ast.location());
    }
}
