package org.tlang.exception;

import org.tlang.ast.AST;

public class TypeException extends Exception {

    public TypeException(String msg, AST ast) {
        super(msg + " " + ast.location());
    }
}
