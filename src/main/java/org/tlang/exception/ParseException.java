package org.tlang.exception;

import org.tlang.lexer.token.Token;

public class ParseException extends Exception {

    public ParseException(String msg, Token token) {
        super("syntax error around " + location(token) + ". " + msg);
    }

    public ParseException(String msg) {
        super(msg);
    }

    private static String location(Token token) {
        if (token == Token.EOF) {
            return "the last line";
        } else {
            return "\"" + token.getText() + "\" at line " + token.getLineNumber();
        }
    }
}
