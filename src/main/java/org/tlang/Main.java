package org.tlang;

import org.tlang.interpreter.SegmentInterpreter;
import org.tlang.interpreter.Interpreter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> file = readFile(args[0]);
        Interpreter interpreter = new SegmentInterpreter();
        try {
            interpreter.eval(file);
        } catch (Throwable e) {
            System.out.println("exception: " + e.getClass().getSimpleName() + ", msg: " + e.getMessage());
        }
    }

    private static List<String> readFile(String fileName) {
        try (InputStream inputStream = Files.newInputStream(Paths.get(fileName));
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader reader = new BufferedReader(inputStreamReader)) {
            return reader.lines().collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("read file exception: " + e.getMessage());
        }
        return new ArrayList<>();
    }

    private static void printFile(List<String> file) {
        for (String line : file) {
            System.out.println(line);
        }
    }
}