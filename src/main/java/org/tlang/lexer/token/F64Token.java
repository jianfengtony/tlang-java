package org.tlang.lexer.token;

public class F64Token extends Token {
    private final double value;

    public F64Token(int lineNumber, double value) {
        super(lineNumber);
        this.value = value;
    }

    @Override
    public String getText() {
        return String.valueOf(value);
    }

    @Override
    public boolean isFloat() {
        return true;
    }

    @Override
    public double getFloat() {
        return value;
    }
}
