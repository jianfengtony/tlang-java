package org.tlang.lexer.token;

import org.tlang.exception.EvalException;

public abstract class Token {
    public static final Token EOF = new Token(-1) {
    };

    public static final String EOL = "\\n";

    private final int lineNumber;

    public Token(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public boolean isIdentifier() {
        return false;
    }

    public boolean isInteger() {
        return false;
    }

    public long getInteger() {
        throw new EvalException("not integer token");
    }

    public boolean isString() {
        return false;
    }

    public String getText() {
        return "";
    }

    public boolean isBool() {
        return false;
    }

    public boolean getBool() {
        throw new EvalException("not boolean token");
    }

    public boolean isFloat() {
        return false;
    }

    public double getFloat() {
        throw new EvalException("not float token");
    }

    @Override
    public String toString() {
        return getText();
    }
}
