package org.tlang.lexer.token;

/**
 * 64bit整数
 */
public class I64Token extends Token {
    private final long value;

    public I64Token(int lineNumber, long value) {
        super(lineNumber);
        this.value = value;
    }

    @Override
    public boolean isInteger() {
        return true;
    }

    @Override
    public String getText() {
        return Long.toString(value);
    }

    @Override
    public long getInteger() {
        return value;
    }
}
