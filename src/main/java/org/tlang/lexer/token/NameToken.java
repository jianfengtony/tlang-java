package org.tlang.lexer.token;

public class NameToken extends Token {
    private final String text;

    public NameToken(int lineNumber, String text) {
        super(lineNumber);
        this.text = text;
    }

    @Override
    public boolean isIdentifier() {
        return true;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
