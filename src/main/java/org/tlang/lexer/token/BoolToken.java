package org.tlang.lexer.token;

import org.tlang.keywords.KeyWords;

public class BoolToken extends Token {
    private final boolean value;

    public BoolToken(int lineNumber, boolean value) {
        super(lineNumber);
        this.value = value;
    }

    @Override
    public boolean getBool() {
        return value;
    }

    @Override
    public boolean isBool() {
        return true;
    }

    @Override
    public String getText() {
        return value ? KeyWords.LITERAL_TRUE : KeyWords.LITERAL_FALSE;
    }
}
