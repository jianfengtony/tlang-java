package org.tlang.lexer.token;

public class StrToken extends Token {
    private final String literal;

    public StrToken(int lineNumber, String str) {
        super(lineNumber);
        this.literal = str;
    }

    @Override
    public boolean isString() {
        return true;
    }

    @Override
    public String getText() {
        return literal;
    }
}
