package org.tlang.ast.list.clazz;

import org.tlang.ast.AST;
import org.tlang.ast.ASTList;
import org.tlang.ast.list.func.FunctionDefine;
import org.tlang.ast.list.statement.VarDefineStatement;
import org.tlang.context.SymbolTable;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.EvalException;

import java.util.List;

public class ClassBody extends ASTList {
    public ClassBody(List<AST> children) {
        super(children);
    }

    @Override
    public void lookup(SymbolTable symbolTable, TypeTable typeTable) {
        for (AST ast : children()) {
            if (ast instanceof VarDefineStatement) {
                ((VarDefineStatement) ast).lookupAsField(symbolTable, typeTable);
            } else if (ast instanceof FunctionDefine) {
                ((FunctionDefine) ast).lookupAsMethod(symbolTable, typeTable);
            } else {
                throw new EvalException("syntax error", ast);
            }
        }
    }

    @Override
    public Object eval(ValueTable valueTable) {
        for (AST ast : children()) {
            if (ast instanceof VarDefineStatement) {
                ast.eval(valueTable);
            } else if (ast instanceof FunctionDefine) {
                ((FunctionDefine) ast).evalAsMethod(valueTable);
            }
        }

        return null;
    }
}
