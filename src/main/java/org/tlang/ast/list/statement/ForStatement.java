package org.tlang.ast.list.statement;

import org.tlang.ast.AST;
import org.tlang.ast.ASTList;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.EvalException;
import org.tlang.exception.TypeException;
import org.tlang.type.BoolType;
import org.tlang.type.Type;
import org.tlang.type.VoidType;

import java.util.List;

/**
 * for_statement: "for" "(" var_define_statement "," condition "," expr ")" block
 */
public class ForStatement extends ASTList {
    public ForStatement(List<AST> children) {
        super(children);
    }

    private AST varDefineStatement() {
        return child(0);
    }

    private AST condition() {
        return child(1);
    }

    private AST expr() {
        return child(2);
    }

    private AST block() {
        return child(3);
    }

    @Override
    public Type typeCheck(TypeTable typeTable) throws TypeException {
        varDefineStatement().typeCheck(typeTable);

        Type conditionType = condition().typeCheck(typeTable);
        conditionType.assertSubTypeOf(BoolType.BOOL_TYPE, typeTable, this);

        expr().typeCheck(typeTable);
        block().typeCheck(typeTable);
        return VoidType.VOID_TYPE;
    }

    @Override
    public Object eval(ValueTable valueTable) {
        Object result = null;
        varDefineStatement().eval(valueTable);

        while (true) {
            Object condition = condition().eval(valueTable);
            if (condition instanceof Boolean) {
                if ((Boolean) condition) {
                    result = block().eval(valueTable);
                    expr().eval(valueTable);
                    continue;
                }

                return result;
            }

            throw new EvalException("condition expression eval result is not boolean", condition());
        }
    }
}
