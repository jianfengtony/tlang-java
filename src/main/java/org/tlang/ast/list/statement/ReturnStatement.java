package org.tlang.ast.list.statement;

import org.tlang.ast.AST;
import org.tlang.ast.ASTList;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.TypeException;
import org.tlang.metaclass.TReturn;
import org.tlang.type.ReturnType;
import org.tlang.type.Type;

import java.util.List;

public class ReturnStatement extends ASTList {
    public ReturnStatement(List<AST> children) {
        super(children);
    }

    private AST expr() {
        return child(0);
    }

    @Override
    public Object eval(ValueTable valueTable) {
        return new TReturn(expr().eval(valueTable));
    }

    @Override
    public String toString() {
        return "( return " + expr() + " )";
    }

    @Override
    public Type typeCheck(TypeTable typeTable) throws TypeException {
        return new ReturnType(expr().typeCheck(typeTable));
    }
}
