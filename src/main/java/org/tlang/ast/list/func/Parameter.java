package org.tlang.ast.list.func;

import org.tlang.ast.AST;
import org.tlang.ast.ASTLeaf;
import org.tlang.ast.ASTList;
import org.tlang.ast.list.type.TypeTag;
import org.tlang.context.TypeTable;
import org.tlang.exception.TypeException;
import org.tlang.type.Type;

import java.util.List;

public class Parameter extends ASTList {
    public Parameter(List<AST> children) {
        super(children);
    }

    String name() {
        return ((ASTLeaf) child(0)).token().getText();
    }

    private TypeTag typeTag() {
        return (TypeTag) child(1);
    }

    public Type type(TypeTable typeTable) {
        return typeTag().type(typeTable);
    }

    @Override
    public Type typeCheck(TypeTable typeTable) throws TypeException {
        return type(typeTable);
    }

    @Override
    public String toString() {
        return name() + ":" + typeTag();
    }
}
