package org.tlang.ast;

import org.tlang.context.SymbolTable;
import org.tlang.context.TypeTable;
import org.tlang.lexer.token.Token;

import java.util.ArrayList;
import java.util.List;

public class ASTLeaf extends AST {
    private static final ArrayList<AST> EMPTY = new ArrayList<>();

    protected Token token;

    public ASTLeaf(Token token) {
        this.token = token;
    }

    @Override
    public AST child(int i) {
        throw new IndexOutOfBoundsException("ASTLeaf has not child");
    }

    @Override
    public int numChildren() {
        return 0;
    }

    @Override
    public List<AST> children() {
        return EMPTY;
    }

    @Override
    public String location() {
        return "at line " + token.getLineNumber();
    }

    @Override
    public void lookup(SymbolTable symbolTable, TypeTable typeTable) {
    }

    public Token token() {
        return token;
    }

    @Override
    public String toString() {
        return token.getText();
    }
}
