package org.tlang.ast;

import org.tlang.context.SymbolTable;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.EvalException;
import org.tlang.exception.TypeException;
import org.tlang.type.Type;

import java.util.List;

public abstract class AST {
    public abstract AST child(int i);

    public abstract int numChildren();

    public abstract List<AST> children();

    public abstract String location();

    public Object eval(ValueTable valueTable) {
        throw new EvalException("can't eval: " + this, this);
    }

    public abstract void lookup(SymbolTable symbolTable, TypeTable typeTable);

    public Type typeCheck(TypeTable typeTable) throws TypeException {
        throw new TypeException("type check not implement of " + this, this);
    }
}
