package org.tlang.ast.leaf;

import org.tlang.ast.ASTLeaf;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.TypeException;
import org.tlang.lexer.token.Token;
import org.tlang.type.IntType;
import org.tlang.type.Type;

public class I64Literal extends ASTLeaf {
    public I64Literal(Token token) {
        super(token);
    }

    private long value() {
        return token.getInteger();
    }

    @Override
    public Object eval(ValueTable valueTable) {
        return value();
    }

    @Override
    public Type typeCheck(TypeTable typeTable) throws TypeException {
        return IntType.INT_TYPE;
    }
}
