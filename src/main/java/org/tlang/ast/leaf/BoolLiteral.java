package org.tlang.ast.leaf;

import org.tlang.ast.ASTLeaf;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.TypeException;
import org.tlang.lexer.token.Token;
import org.tlang.type.BoolType;
import org.tlang.type.Type;

public class BoolLiteral extends ASTLeaf {
    public BoolLiteral(Token token) {
        super(token);
    }

    private boolean value() {
        return token.getBool();
    }

    @Override
    public Object eval(ValueTable valueTable) {
        return value();
    }

    @Override
    public Type typeCheck(TypeTable typeTable) throws TypeException {
        return BoolType.BOOL_TYPE;
    }
}
