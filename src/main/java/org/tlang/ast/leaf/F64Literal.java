package org.tlang.ast.leaf;

import org.tlang.ast.ASTLeaf;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.TypeException;
import org.tlang.lexer.token.Token;
import org.tlang.type.FloatType;
import org.tlang.type.Type;

public class F64Literal extends ASTLeaf {
    public F64Literal(Token token) {
        super(token);
    }

    private double value() {
        return token.getFloat();
    }

    @Override
    public Object eval(ValueTable valueTable) {
        return value();
    }

    @Override
    public Type typeCheck(TypeTable typeTable) throws TypeException {
        return FloatType.FLOAT_TYPE;
    }
}
