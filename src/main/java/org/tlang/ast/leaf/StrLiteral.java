package org.tlang.ast.leaf;

import org.tlang.ast.ASTLeaf;
import org.tlang.context.TypeTable;
import org.tlang.context.ValueTable;
import org.tlang.exception.TypeException;
import org.tlang.lexer.token.Token;
import org.tlang.type.StrType;
import org.tlang.type.Type;

public class StrLiteral extends ASTLeaf {
    public StrLiteral(Token token) {
        super(token);
    }

    private String value() {
        return token.getText();
    }

    @Override
    public Object eval(ValueTable valueTable) {
        return value();
    }

    @Override
    public Type typeCheck(TypeTable typeTable) throws TypeException {
        return StrType.STR_TYPE;
    }
}
