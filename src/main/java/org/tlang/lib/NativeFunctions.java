package org.tlang.lib;

public class NativeFunctions {
    public static void println(String str) {
        System.out.println(str);
    }

    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public static long nanoTime() {
        return System.nanoTime();
    }

    public static String intToStr(long i) {
        return String.valueOf(i);
    }

    public static String boolToStr(boolean bool) {
        return String.valueOf(bool);
    }

    public static String floatToStr(double d) {
        return String.valueOf(d);
    }
}
