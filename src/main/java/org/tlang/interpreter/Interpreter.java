package org.tlang.interpreter;

import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

import java.util.List;

public interface Interpreter {
    Object eval(String text) throws ParseException, TypeException;

    Object eval(List<String> texts) throws ParseException, TypeException;
}
