package org.tlang.interpreter;

import org.tlang.lexer.Lexer;
import org.tlang.parser.ExprParser;
import org.tlang.parser.Parser;

public class ExprInterpreter extends AbsInterpreter {

    @Override
    protected Parser createParser(Lexer lexer) {
        return new ExprParser(lexer);
    }
}
