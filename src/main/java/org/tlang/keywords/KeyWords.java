package org.tlang.keywords;

public class KeyWords {
    public static final String VAR = "var";
    public static final String FUNC = "func";
    public static final String CONSTRUCTOR = "constructor";
    public static final String NEW = "new";
    public static final String ARRAY = "Array";
    public static final String CLASS = "class";
    public static final String EXTENDS = "extends";
    public static final String RETURN = "return";
    public static final String IF = "if";
    public static final String ELSE = "else";
    public static final String WHILE = "while";
    public static final String THIS = "this";
    public static final String INSTANCE_OF = "instanceof";
    public static final String CONTINUE = "continue";
    public static final String BREAK = "break";
    public static final String FOR = "for";
    public static final String SWITCH = "switch";
    public static final String CASE = "case";
    public static final String DEFAULT = "default";
    public static final String SUPER = "super";
    public static final String INTERFACE = "interface";
    public static final String IMPLEMENTS = "implements";
    public static final String PUBLIC = "public";
    public static final String PRIVATE = "private";
    public static final String PROTECT = "protect";
    public static final String IMPORT = "import";
    public static final String PACKAGE = "package";

    public static final String LITERAL_TRUE = "true";
    public static final String LITERAL_FALSE = "false";

    public static final String TYPE_INT = "int";
    public static final String TYPE_BOOL = "bool";
    public static final String TYPE_STR = "string";
    public static final String TYPE_VOID = "void";
    public static final String TYPE_NUMBER = "number";
    public static final String TYPE_FLOAT = "float";
}
