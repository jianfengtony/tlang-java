package org.tlang.context;

public class Context {
    private final SymbolTable symbolTable;
    private final TypeTable typeTable;
    private final ValueTable valueTable;

    public Context() {
        this.symbolTable = new SymbolTable();
        this.typeTable = new TypeTable(2, "global");
        this.valueTable = new ValueTable(2, "global");
    }

    public SymbolTable symbolTable() {
        return symbolTable;
    }

    public TypeTable typeTable() {
        return typeTable;
    }

    public ValueTable valueTable() {
        return valueTable;
    }
}
