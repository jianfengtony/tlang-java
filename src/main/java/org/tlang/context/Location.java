package org.tlang.context;

public class Location {
    public static final int UNKNOWN = -1;

    private final int nest;
    private final int index;

    public Location(int nest, int index) {
        this.nest = nest;
        this.index = index;
    }

    public int nest() {
        return nest;
    }

    public int index() {
        return index;
    }

    @Override
    public String toString() {
        return "nest=" + nest + ", index=" + index;
    }
}
