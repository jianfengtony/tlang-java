package org.tlang.interpreter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.FileUtils;
import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

import java.util.List;

public class ArrayTest {
    private Interpreter interpreter;

    @Before
    public void setUp() {
        interpreter = new SegmentInterpreter();
    }

    @Test
    public void array_smoking_test() throws ParseException, TypeException {
        List<String> lines = FileUtils.readFile("array_smoking_test.tl");
        List<?> result = (List<?>) interpreter.eval(lines);

        Assert.assertArrayEquals(new Long[]{1L, 11L, 3L}, (Object[]) result.get(1));
        Assert.assertEquals(11L, result.get(2));
    }
}