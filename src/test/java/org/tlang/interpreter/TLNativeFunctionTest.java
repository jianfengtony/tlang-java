package org.tlang.interpreter;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TLNativeFunctionTest {
    private Interpreter interpreter;
    private ByteArrayOutputStream outContent;
    private PrintStream originalOut;

    @Before
    public void setUp() {
        interpreter = new SegmentInterpreter();
        originalOut = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void println_test() throws ParseException, TypeException {
        String program = "println(\"Hello Native\");";
        interpreter.eval(program);
        String result = outContent.toString();
        Assert.assertEquals("Hello Native" + System.lineSeparator(), result);
    }

    @Test
    public void nano_time_test() throws TypeException, ParseException {
        String program = "nanoTime();";
        Object result = interpreter.eval(program);
        Assert.assertTrue(result instanceof Long);
        Assert.assertTrue((Long) result > 0);
    }

    @Test
    public void int_to_str_test() throws TypeException, ParseException {
        String program = "println(intToStr(10));";
        interpreter.eval(program);
        String result = outContent.toString();
        Assert.assertEquals("10" + System.lineSeparator(), result);
    }

    @Test
    public void bool_to_str_test() throws TypeException, ParseException {
        String program = "println(boolToStr(true));";
        interpreter.eval(program);
        String result = outContent.toString();
        Assert.assertEquals("true" + System.lineSeparator(), result);
    }
}
