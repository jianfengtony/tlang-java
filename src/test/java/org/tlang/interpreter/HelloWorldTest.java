package org.tlang.interpreter;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.FileUtils;
import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

public class HelloWorldTest {
    private Interpreter interpreter;
    private ByteArrayOutputStream outContent;
    private PrintStream originalOut;

    @Before
    public void setUp() {
        interpreter = new SegmentInterpreter();
        originalOut = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void hello_world_test() throws TypeException, ParseException {
        List<String> lines = FileUtils.readFile("hello_world.tl");
        interpreter.eval(lines);
        Assert.assertEquals("" +
                "hello tlang!" + System.lineSeparator() +
                "1" + System.lineSeparator() +
                "a < 10" + System.lineSeparator() +
                "0" + System.lineSeparator() +
                "1" + System.lineSeparator() +
                "0" + System.lineSeparator() +
                "1" + System.lineSeparator() +
                "2" + System.lineSeparator() +
                "2" + System.lineSeparator() +
                "3" + System.lineSeparator() +
                "4" + System.lineSeparator() +
                "5" + System.lineSeparator() +
                "6" + System.lineSeparator() +
                "7" + System.lineSeparator(), outContent.toString());
    }
}
