package org.tlang.interpreter;

import org.junit.Assert;
import org.junit.Test;
import org.tlang.AbsTest;
import org.tlang.FileUtils;
import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

import java.util.List;

public class ClassTest extends AbsTest {

    @Test
    public void class_smoking_test() throws ParseException, TypeException {
        List<String> lines = FileUtils.readFile("class_smoking_test.tl");
        Object result = interpreter.eval(lines);
        int i = -1;
        List<?> listResult = (List<?>) result;
        Assert.assertEquals("Math", listResult.get(++i));
        Assert.assertEquals("math", listResult.get(++i));
        Assert.assertEquals("x", listResult.get(++i));
        Assert.assertEquals("result", listResult.get(++i));
        Assert.assertEquals("memberA", listResult.get(++i));
        Assert.assertEquals("memberC", listResult.get(++i));
        Assert.assertEquals(8L, listResult.get(++i));
        Assert.assertEquals(100L, listResult.get(++i));
        Assert.assertEquals(101L, listResult.get(++i));
        Assert.assertNull(listResult.get(++i));
        Assert.assertEquals(3L, listResult.get(++i));

        Assert.assertEquals("MathWithConstructor", listResult.get(++i));
        Assert.assertEquals("mathWithConstructor", listResult.get(++i));
        Assert.assertEquals("temp", listResult.get(++i));
        Assert.assertEquals(7L, listResult.get(++i));

        Assert.assertEquals("MathExtend", listResult.get(++i));
        Assert.assertEquals("mathExt", listResult.get(++i));
        Assert.assertEquals(6L, listResult.get(++i));

        Assert.assertEquals("superMath", listResult.get(++i));
        Assert.assertEquals(3L, listResult.get(++i));
    }

    @Test
    public void class_nest_member_test() throws TypeException, ParseException {
        List<String> lines = FileUtils.readFile("class_nest_member_access_test.tl");
        interpreter.eval(lines);
        Assert.assertEquals("2\n" +
                "2\n" +
                "100\n" +
                "5\n" +
                "5\n", outContent.toString());
    }
}