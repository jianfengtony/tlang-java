package org.tlang.interpreter;

import org.junit.Assert;
import org.junit.Test;
import org.tlang.AbsTest;
import org.tlang.FileUtils;
import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

import java.util.Arrays;
import java.util.List;

public class TFunctionTest extends AbsTest {

    @Test
    public void function_smoking_test() throws ParseException, TypeException {
        List<String> lines = FileUtils.readFile("function_smoking_test.tl");
        List<?> result = (List<?>) interpreter.eval(lines);
        int i = 0;
        Assert.assertEquals("add", result.get(i++));
        Assert.assertEquals("x", result.get(i++));
        Assert.assertEquals("y", result.get(i++));
        Assert.assertEquals("c", result.get(i));
        Assert.assertEquals("3" + System.lineSeparator(), outContent.toString());
    }

    @Test
    public void fibonacci_function_test() throws ParseException, TypeException {
        List<String> lines = FileUtils.readFile("function_fibonacci_test.tl");
        Object result = interpreter.eval(lines);
        Assert.assertEquals(Arrays.asList("fibonacci", 55L), result);
    }

    @Test
    public void exponential_function_test() throws ParseException, TypeException {
        List<String> lines = FileUtils.readFile("function_exponential_test.tl");
        Object result = interpreter.eval(lines);
        Assert.assertEquals(Arrays.asList("exponential", 1024L), result);
    }

    @Test
    public void function_return_void_test() throws TypeException, ParseException {
        List<String> lines = FileUtils.readFile("function_return_void_test.tl");
        Object result = interpreter.eval(lines);
        Assert.assertEquals(Arrays.asList("printInt", null), result);
    }
}