package org.tlang.interpreter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

public class ExprTest {
    private Interpreter interpreter;

    @Before
    public void setUp() {
        interpreter = new ExprInterpreter();
    }

    @Test
    public void add_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("1 + 2 + 3");
        Assert.assertEquals(6L, result);
    }

    @Test
    public void minus_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("2 -1 ");
        Assert.assertEquals(1L, result);
    }

    @Test
    public void add_string_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("\"abc\" + \"def\"");
        Assert.assertEquals("abcdef", result);
    }

    @Test
    public void multiply_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("2 * 2");
        Assert.assertEquals(4L, result);
    }

    @Test
    public void equal_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval(" 2 == 2");
        Assert.assertTrue((Boolean) result);
    }

    @Test
    public void not_equal_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval(" 3 != 2");
        Assert.assertTrue((Boolean) result);
    }

    @Test
    public void negative_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("-(1+1)");
        Assert.assertEquals(-2L, result);
    }

    @Test
    public void logic_not_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("!(2!=2)");
        Assert.assertTrue((Boolean) result);
    }

    @Test
    public void logic_and_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("1==1 && 2==2");
        Assert.assertTrue((Boolean) result);
    }

    @Test
    public void logic_or_smoking_test() throws ParseException, TypeException {
        Object result = interpreter.eval("1==1 || 2==2");
        Assert.assertTrue((Boolean) result);
    }

    @Test
    public void bool_expr_test() throws ParseException, TypeException {
        Object result = interpreter.eval("true && true");
        Assert.assertTrue((Boolean) result);

        result = interpreter.eval("true && false");
        Assert.assertFalse((Boolean) result);

        result = interpreter.eval("false || true");
        Assert.assertTrue((Boolean) result);

        result = interpreter.eval("false || false");
        Assert.assertFalse((Boolean) result);

        result = interpreter.eval("!true");
        Assert.assertFalse((Boolean) result);

        result = interpreter.eval("!false");
        Assert.assertTrue((Boolean) result);

        result = interpreter.eval("true == (1 == 1)");
        Assert.assertTrue((Boolean) result);

        result = interpreter.eval("true != (1 != 1)");
        Assert.assertTrue((Boolean) result);
    }

    @Test
    public void float_expr_test() throws TypeException, ParseException {
        Object result = interpreter.eval("1.0 + 2.0");
        Assert.assertEquals(3.0, (Double) result, 0.01);
    }
}