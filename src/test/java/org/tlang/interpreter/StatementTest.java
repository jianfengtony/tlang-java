package org.tlang.interpreter;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.FileUtils;
import org.tlang.metaclass.TReturn;
import org.tlang.exception.ParseException;
import org.tlang.exception.TypeException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

public class StatementTest {
    private Interpreter interpreter;
    private ByteArrayOutputStream outContent;
    private PrintStream originalOut;

    @Before
    public void setUp() {
        interpreter = new SegmentInterpreter();
        originalOut = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(originalOut);
    }

    @Test
    public void simple_statement_smoking_test() throws ParseException, TypeException {
        String statement = "1 + 1;";
        Object result = interpreter.eval(statement);
        Assert.assertEquals(2L, result);
    }

    @Test
    public void if_statement_smoking_test() throws ParseException, TypeException {
        String statement = "" +
                "   var c: int = 0;         " +
                "   if (1 == 1) {           " +
                "       var a: int = 1 + 1; " +
                "       var b: int = a + 1; " +
                "       c = b + 1;          " +
                "   } else {                " +
                "       var x: int = 1 + 1; " +
                "       c = x + 1;          " +
                "   }                       " +
                "   c;                      ";
        Object result = interpreter.eval(statement);
        List<?> resultList = (List<?>) result;
        int i = -1;
        Assert.assertEquals("c", resultList.get(++i));
        Assert.assertEquals(TReturn.VOID, resultList.get(++i));
        Assert.assertEquals(4L, resultList.get(++i));
    }

    @Test
    public void if_statement_condition_false_test() throws ParseException, TypeException {
        String statement = "" +
                "   var c: int;                 " +
                "   if (1 != 1) {               " +
                "       var a: int = 1 + 1;     " +
                "       var b: int = a + 1;     " +
                "       c = b + 1;              " +
                "   } else {                    " +
                "       var x: int = 1 + 1;     " +
                "       c = x + 1;              " +
                "   }                           " +
                "   c;                          ";
        Object result = interpreter.eval(statement);
        List<?> resultList = (List<?>) result;
        int i = -1;
        Assert.assertEquals("c", resultList.get(++i));
        Assert.assertEquals(TReturn.VOID, resultList.get(++i));
        Assert.assertEquals(3L, resultList.get(++i));
    }

    @Test
    public void while_statement_smoking_test() throws ParseException, TypeException {
        List<String> lines = FileUtils.readFile("while_statement_test.tl");
        Object result = interpreter.eval(lines);
        List<?> resultList = (List<?>) result;
        int i = -1;
        Assert.assertEquals("a", resultList.get(++i));
        Assert.assertEquals("sum", resultList.get(++i));
        Assert.assertEquals(TReturn.VOID, resultList.get(++i));
        Assert.assertEquals(55L, resultList.get(++i));
    }

    @Test
    public void var_define_statement_smoking_test() throws ParseException, TypeException {
        String statement = "var a: int = 0;";
        Object result = interpreter.eval(statement);
        Assert.assertEquals("a", result);
    }

    @Test
    public void var_define_statement_of_string_test() throws ParseException, TypeException {
        String statement = "var a: string = \"hello\";";
        Object result = interpreter.eval(statement);
        Assert.assertEquals("a", result);
    }

    @Test
    public void var_define_statement_of_bool_test() throws ParseException, TypeException {
        String statement = "var a: bool = false;";
        Object result = interpreter.eval(statement);
        Assert.assertEquals("a", result);
    }

    @Test
    public void var_define_statement_of_float_test() throws ParseException, TypeException {
        String statement = "var a: float = 8.8;";
        Object result = interpreter.eval(statement);
        Assert.assertEquals("a", result);
    }

    @Test
    public void for_statement_smoking_test() throws ParseException, TypeException {
        List<String> lines = FileUtils.readFile("for_statement_test.tl");
        interpreter.eval(lines);
        Assert.assertEquals("0" + System.lineSeparator() +
                "1" + System.lineSeparator() +
                "2" + System.lineSeparator(), outContent.toString());
    }
}