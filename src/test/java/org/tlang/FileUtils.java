package org.tlang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtils {
    private static final String path = "src/test/tlang/";

    public static List<String> readFile(String fileName) {
        try (InputStream inputStream = Files.newInputStream(Paths.get(path + fileName));
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader reader = new BufferedReader(inputStreamReader)) {
            return reader.lines().collect(Collectors.toList());
        } catch (IOException ignored) {
        }
        return new ArrayList<>();
    }
}
