package org.tlang.lexer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.exception.ParseException;
import org.tlang.lexer.token.Token;

public class LexerTest {
    Lexer lexer;

    @Before
    public void setUp() {
        lexer = new Lexer();
    }

    private static void assertIdToken(Token token, String id, int lineNumber) {
        Assert.assertTrue(token.isIdentifier());
        Assert.assertEquals(id, token.getText());
        Assert.assertEquals(lineNumber, token.getLineNumber());
    }

    private static void assertNumToken(Token token, int num, int lineNumber) {
        Assert.assertTrue(token.isInteger());
        Assert.assertEquals(num, token.getInteger());
        Assert.assertEquals(lineNumber, token.getLineNumber());
        Assert.assertEquals(String.valueOf(num), token.getText());
    }

    private static void assertStrToken(Token token, String str, int lineNumber) {
        Assert.assertTrue(token.isString());
        Assert.assertEquals(str, token.getText());
        Assert.assertEquals(lineNumber, token.getLineNumber());
    }

    @Test
    public void parse_number_smoking_test() throws ParseException {
        lexer.parse("123 + 456");

        assertNumToken(lexer.peek(0), 123, 0);
        assertIdToken(lexer.peek(1), "+", 0);
        assertNumToken(lexer.peek(2), 456, 0);
    }

    @Test
    public void parse_identifier_smoking_test() throws ParseException {
        lexer.parse("abc + b1");

        assertIdToken(lexer.poll(), "abc", 0);
        assertIdToken(lexer.poll(), "+", 0);
        assertIdToken(lexer.poll(), "b1", 0);
    }

    @Test
    public void parse_string_smoking_test() throws ParseException {
        lexer.parse("\"abc\" + \"def\"");

        assertStrToken(lexer.poll(), "abc", 0);
        assertIdToken(lexer.poll(), "+", 0);
        assertStrToken(lexer.poll(), "def", 0);
    }

    @Test
    public void parse_string_with_back_slash_test() throws ParseException {
        lexer.parse("\"\\\\\"");

        assertStrToken(lexer.poll(), "\\", 0);
    }

    @Test
    public void parse_string_with_return_test() throws ParseException {
        lexer.parse("\"\\n\"");

        assertStrToken(lexer.poll(), "\n", 0);
    }

    @Test
    public void parse_string_with_quotes_test() throws ParseException {
        lexer.parse("\"\\\"\"");

        assertStrToken(lexer.poll(), "\"", 0);
    }

    @Test
    public void invalid_pattern_test() throws ParseException {
        lexer.parse("\"\\t\"");

        assertStrToken(lexer.poll(), "\\t", 0);
    }

    @Test
    public void parse_bool_smoking() throws ParseException {
        lexer.parse("true false true1 false1");
        Token token = lexer.poll();
        Assert.assertTrue(token.isBool());
        Assert.assertTrue(token.getBool());

        token = lexer.poll();
        Assert.assertTrue(token.isBool());
        Assert.assertFalse(token.getBool());

        token = lexer.poll();
        Assert.assertFalse(token.isBool());

        token = lexer.poll();
        Assert.assertFalse(token.isBool());
    }

    @Test
    public void self_add_and_minus_test() throws ParseException {
        lexer.parse("++a --b");
        Token token = lexer.poll();
        Assert.assertTrue(token.isIdentifier());
        Assert.assertEquals("++", token.getText());
        Assert.assertEquals("a", lexer.poll().getText());

        token = lexer.poll();
        Assert.assertTrue(token.isIdentifier());
        Assert.assertEquals("--", token.getText());
        Assert.assertEquals("b", lexer.poll().getText());
    }

    @Test
    public void float_number_test() throws ParseException {
        lexer.parse("a = 1.0");
        Token token = lexer.peek(2);
        Assert.assertEquals("1.0", token.getText());
        Assert.assertEquals(1.0, token.getFloat(), 0.01);
    }
}