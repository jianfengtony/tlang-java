package org.tlang.type;

import org.junit.Assert;
import org.junit.Test;

public class FuncTypeTest {

    @Test
    public void to_string_test() {
        FuncType type = new FuncType("add", IntType.INT_TYPE, new Type[]{IntType.INT_TYPE, StrType.STR_TYPE});
        Assert.assertEquals("add(int,string) -> int", type.toString());
    }
}