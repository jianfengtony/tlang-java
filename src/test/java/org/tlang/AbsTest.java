package org.tlang;

import org.junit.After;
import org.junit.Before;
import org.tlang.interpreter.Interpreter;
import org.tlang.interpreter.SegmentInterpreter;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class AbsTest {
    protected Interpreter interpreter;
    protected ByteArrayOutputStream outContent;
    private PrintStream originalOut;

    @Before
    public void setUp() {
        interpreter = new SegmentInterpreter();
        originalOut = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(originalOut);
    }

}
