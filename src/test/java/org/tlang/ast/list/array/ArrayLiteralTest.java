package org.tlang.ast.list.array;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.lexer.token.I64Token;
import org.tlang.ast.AST;
import org.tlang.ast.ASTLeaf;

import java.util.ArrayList;
import java.util.List;

public class ArrayLiteralTest {
    private ArrayLiteral arrayLiteral;

    @Before
    public void setUp() {
        List<AST> astList = new ArrayList<>();
        astList.add(new ASTLeaf(new I64Token(0, 1)));
        astList.add(new ASTLeaf(new I64Token(0, 2)));
        astList.add(new ASTLeaf(new I64Token(0, 3)));
        arrayLiteral = new ArrayLiteral(astList);
    }

    @Test
    public void to_string_test() {
        Assert.assertEquals("( [1, 2, 3] )", arrayLiteral.toString());
    }
}