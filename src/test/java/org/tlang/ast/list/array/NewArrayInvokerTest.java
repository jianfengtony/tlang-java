package org.tlang.ast.list.array;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.ast.AST;
import org.tlang.ast.leaf.Name;
import org.tlang.ast.leaf.I64Literal;
import org.tlang.context.ValueTable;
import org.tlang.lexer.token.NameToken;
import org.tlang.lexer.token.I64Token;

import java.util.ArrayList;
import java.util.List;

public class NewArrayInvokerTest {
    private NewArrayInvoker arrayInvoker;

    @Before
    public void setUp() {
    }

    @Test
    public void dim_1_test() {
        List<AST> astList = new ArrayList<>();
        astList.add(new Name(new NameToken(0, "int")));
        astList.add(new I64Literal(new I64Token(0, 3)));
        arrayInvoker = new NewArrayInvoker(astList);

        Assert.assertEquals("new int[3]", arrayInvoker.toString());
        Object result = arrayInvoker.eval(new ValueTable(8, "dim_1_test"));
        Assert.assertTrue(result instanceof Object[]);
    }

    @Test
    public void dim_2_test() {
        List<AST> astList = new ArrayList<>();
        astList.add(new Name(new NameToken(0, "int")));
        astList.add(new I64Literal(new I64Token(0, 3)));
        astList.add(new I64Literal(new I64Token(0, 4)));
        arrayInvoker = new NewArrayInvoker(astList);

        Assert.assertEquals("new int[3][4]", arrayInvoker.toString());
        Object result = arrayInvoker.eval(new ValueTable(8, "dim_2_test"));
        Assert.assertTrue(result instanceof Object[]);
    }
}