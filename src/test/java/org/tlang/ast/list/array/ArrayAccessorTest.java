package org.tlang.ast.list.array;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tlang.lexer.token.NameToken;
import org.tlang.lexer.token.I64Token;
import org.tlang.ast.AST;
import org.tlang.ast.ASTLeaf;

import java.util.ArrayList;
import java.util.List;

public class ArrayAccessorTest {
    private ArrayAccessor arrayAccessor;

    @Before
    public void setUp() {
        List<AST> astList = new ArrayList<>();
        astList.add(new ASTLeaf(new NameToken(0, "arr")));
        astList.add(new ASTLeaf(new I64Token(0, 1)));
        astList.add(new ASTLeaf(new I64Token(0, 2)));
        astList.add(new ASTLeaf(new I64Token(0, 3)));
        arrayAccessor = new ArrayAccessor(astList);
    }

    @Test
    public void to_string_test() {
        Assert.assertEquals("( arr[1][2][3] )", arrayAccessor.toString());
    }
}