# TLang

#### 介绍
T Lang，简称TL，是一种新型的、现代的、简洁的编程语言，T代表了“科技Technology”，Lang是英文单词“语言/Language”的意思。

#### 软件架构
本项目采用JAVA 8编写，目的是为了便于移植和嵌入小型系统。


#### 安装教程

编译打包结果为一个单一的jar包，通过命令行执行tlang脚本如下：

```sh
java -jar tlang.jar xxx.tl
```


#### 使用说明

```javascript
// hello word
println("hello tlang!");

// expr
var a: int = 0;
var b: int = 1;
println(intToStr(a + b));

// control statement
if (a < 10) {
    println("a < 10");
} else if (10 < a && a < 20) {
    println("10 < a && a < 20");
} else {
    println("a >= 20");
}

a = 0;
while (a < 2) {
    println(intToStr(a));
    a = a + 1;
}

for (var i: int = 0; i < 3; i = i + 1) {
    println(intToStr(i));
}

// function
func add(a: int, b: int): int {
    return a + b;
}
var c: int = add(1, 1);
println(intToStr(c));

// class
class A {
    var a: int;
    var b: int;

    constructor(a: int, b: int) {
        this.a = a;
        this.b = b;
    }

    func setA(a: int): void {
        this.a = a;
    }

    func setB(b: int): void {
        this.b = b;
    }

    func add(): int {
        return this.a + this.b;
    }
}

var classA: A = new A(1, 1);
classA.setB(2);
println(intToStr(classA.add())); // 1+2=3

// array
var arr: int[][] = [[4, 2, 3], [4, 5, 6]];
println(intToStr(arr[0][0]));
println(intToStr(arr[1][1]));

// extends
class B extends A {
    var c: int;

    func setC(c: int): void {
        this.c = c;
    }

    func multiAdd(): int {
        return ( this.a + this.b ) * this.c;
    }
}

var classB: B = new B(2, 4);
classB.setC(2);
println(intToStr(classB.add()));          // 2 + 4
println(intToStr(classB.multiAdd() - 5)); // (2 + 4) * 2 - 5
```

上述代码片段的运行结果如下：
下面第一行代码为mac下命令行调用方式，后面的是命令行下输出的执行结果：

```shell
% java -jar tlang.jar hello_world.tl

hello tlang!
1
a < 10
0
1
0
1
2
2
3
4
5
6
7
```

#### 参与贡献

该项目处于起步阶段，如您对该项目感兴趣，请发邮件联系: jianfengtony@163.com
